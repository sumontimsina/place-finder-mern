import React, {useEffect , useState } from "react";
import UsersList from "../components/UsersList";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import {useHttpClient} from "../../shared/hooks/http-Hook";

const Users = () =>{
    const {isLoading, error, sendRequest, clearError } = useHttpClient()
    const[loadedUsers, setLoadedUsers] = useState(null);
    useEffect(()=>{
        const fetchUsers = async()=>{
            try{
                const responseData = await sendRequest("http://localhost:5000/api/users");
                setLoadedUsers(responseData.users);
            }catch(err){
            }            
        }
    fetchUsers();
    }, [sendRequest]);


    return (
        <React.Fragment>
            <ErrorModal error = {error} onClear = {clearError}></ErrorModal>
            {isLoading && (
                <div className = "center">
                    <LoadingSpinner asOverLay></LoadingSpinner>
                </div>)
            }
            {!isLoading && loadedUsers && <UsersList items={loadedUsers} /> }
        </React.Fragment>);
};

export default Users;