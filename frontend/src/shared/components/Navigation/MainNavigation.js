import React, { Fragment , useState } from "react";
import {Link } from  "react-router-dom";
import "./MainNavigation.css"; 
import ManinHeader from "./MainHeader";
import SideDrawer from "./SideDrawer";
import NavLinks from "./NavLinks";
import Backdrop from "../UIElements/Backdrop";

const MainNavigation = () =>{
    const [drawerIsOpen, setDawerIsOpen] = useState(false);

    const openDrawerHandler = () =>{
        setDawerIsOpen(true);
    };
    const closeDrawerHandler = () =>{
        setDawerIsOpen(false);
    };

    return (
        <Fragment>
        {drawerIsOpen && <Backdrop onClick = {closeDrawerHandler}/> }
        <SideDrawer show = {drawerIsOpen} onClick = {closeDrawerHandler}>
            <nav className = "main-navigation__drawer-nav">
                <NavLinks></NavLinks>
            </nav>
        </SideDrawer>}

        <ManinHeader>
        <button className="main-navigation__menu-btn" onClick={openDrawerHandler}>
            <span />
            <span />
            <span />
        </button>
        <h1 className = "main-navigation__title">
            <Link to ="/">Your Places</Link>
        </h1>
        <nav className = "main-navigation__header-nav">
           <NavLinks/>
        </nav>
    </ManinHeader>
    </Fragment>
    );
};

export default MainNavigation;