import React , {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import PlaceList from "../components/PlaceList";
import {useHttpClient} from "../../shared/hooks/http-Hook"; 
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner"
import ErrorModal from "../../shared/components/UIElements/ErrorModal";


const UserPlaces = () => {
    const{isLoading, error, sendRequest, clearError} = useHttpClient();
    const[loadedPlaces, setLoadedPlaces] = useState()
    const userId = useParams().userId;
    useEffect(()=>{
        const fetchPlaces = async ()=>{
            try{
                const responsedata = await sendRequest(`http://localhost:5000/api/places/user/${userId}`);
                console.log(responsedata);
                setLoadedPlaces(responsedata.place)
            }catch(err){

            }
        }
        fetchPlaces();
    },[sendRequest]); 


    const placedeletedHandler = (deletedPlaceId)=>{
        setLoadedPlaces(prevPlaces=>prevPlaces.filter(place => place.id !== deletedPlaceId));
    }
    return (
        <React.Fragment>
            <ErrorModal error= {error} onClear = {clearError}></ErrorModal>
            {isLoading && (
                <div className = "center">
                    <LoadingSpinner/>
                </div>
                )}
            {!isLoading && loadedPlaces && <PlaceList items ={loadedPlaces} onDeletePlace = {placedeletedHandler}/>}
        
        </React.Fragment>
        )

};

export default UserPlaces;